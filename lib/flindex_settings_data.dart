import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';

import 'flindex_utils.dart';

const String dataFileName = "settings.json";

class FlindexSettingsData with ChangeNotifier {
  bool loaded = false;
  dynamic data;

  String get theme {
    if (loaded) {
      if (data.containsKey('theme')) {
        return data['theme'];
      }
    }
    return "system";
  }

  ThemeMode get themeMode {
    if (loaded) {
      switch (data['theme']) {
        case 'light':
          return ThemeMode.light;
        case 'dark':
          return ThemeMode.dark;
      }
    }
    return ThemeMode.system;
  }

  void loadData() async {
    String json = '{}';
    final file = await dataFile("settings.json");
    log('Loading data from file: $file');
    final exists = await file.exists();
    if (exists) {
      json = await file.readAsString();
    }
    data = jsonDecode(json);
    loaded = true;
    notifyListeners();
  }

  void saveData() async {
    if (!loaded) return;
    final file = await dataFile(dataFileName);
    file.open(mode: FileMode.write);
    var payload = jsonEncode(data);
    log('Writing payload to file $file: $payload');
    file.writeAsString(payload);
    notifyListeners();
  }

  void setTheme(String theme) async {
    if (!loaded) return;
    log("Setting theme to $theme");
    data['theme'] = theme;
    saveData();
  }
}
