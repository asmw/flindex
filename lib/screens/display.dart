import 'package:flindex/flindex_data.dart';
import 'package:flindex/screens/edit.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FlindexDisplay extends StatelessWidget {
  const FlindexDisplay({Key? key, required this.code, this.highlight}) : super(key: key);

  final String code;
  final String? highlight;

  void _edit(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        settings: const RouteSettings(name: '/edit'),
        builder: (context) => FlindexEdit(code: code),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<FlindexData>(builder: (context, data, child) {
      return Scaffold(
        appBar: AppBar(
          title: Text(data.prettyName(code)),
          actions: [
            IconButton(
                onPressed: () => _edit(context), icon: const Icon(Icons.edit))
          ],
        ),
        body: Center(
          child: ListView.builder(
              padding: const EdgeInsets.all(8),
              itemCount: data.count(code),
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  padding: const EdgeInsets.all(1),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius:
                          const BorderRadius.all(Radius.circular(2.0)),
                          border: Border.all(color: highlight == data.items(code, index) ? Theme.of(context).indicatorColor : Theme.of(context).backgroundColor),
                      color: Theme.of(context).backgroundColor,
                    ),
                    height: 50,
                    child: Row(children: [
                      const SizedBox(width: 8),
                      Text('${data.items(code, index)}'),
                      const Spacer(),
                    ]),
                  ),
                );
              }),
        ),
      );
    });
  }
}
