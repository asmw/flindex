import 'dart:developer';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flindex/flindex_data.dart';
import 'package:flindex/flindex_settings_data.dart';
import 'package:flindex/flindex_utils.dart';
import 'package:flindex/screens/flindex_theme_selector.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:settings_ui/settings_ui.dart';

import 'flindex_drawer.dart';

class FlindexSettings extends StatelessWidget {
  const FlindexSettings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const FlindexDrawer(),
      appBar: AppBar(
        title: const Text('Flindex settings'),
      ),
      body: SettingsList(
        sections: [
          SettingsSection(
            title: const Text('UI'),
            tiles: <SettingsTile>[
              SettingsTile(
                  leading: const Icon(Icons.format_paint),
                  title: const Text('Theme'),
                  description: const Text('Choose the theme for Flindex'),
                  value: Text(Provider.of<FlindexSettingsData>(context).theme),
                  onPressed: (context) {
                    Navigator.of(context).push(MaterialPageRoute(
                      settings: const RouteSettings(name: "/themes"),
                      builder: (_) => const FlindexThemeSelector(),
                    ));
                  }),
            ],
          ),
          SettingsSection(
            title: const Text('Data'),
            tiles: <SettingsTile>[
              SettingsTile(
                  leading: const Icon(Icons.save),
                  title: const Text('Export data'),
                  description: const Text('Export your flindex data'),
                  onPressed: (_) async {
                    bool save = true;
                    if (await needManageStoragePermission()) {
                      var permission =
                          await Permission.manageExternalStorage.request();
                      log(permission.toString());
                      save = permission.isGranted;
                    } else {
                      var permission = await Permission.storage.request();
                      log(permission.toString());
                      save = permission.isGranted;
                    }
                    if (save) {
                      String? selectedDirectory =
                          await FilePicker.platform.getDirectoryPath();

                      if (selectedDirectory != null) {
                        log("Saving data to: $selectedDirectory");
                        var data = await dataFile("data.json");
                        if (data.existsSync()) {
                          var dataResult = await data
                              .copy("$selectedDirectory/flindex_data.json");
                          if (await dataResult.exists()) {
                            log("Data saved successfully");
                          }
                        }

                        var settings = await dataFile("settings.json");
                        if (settings.existsSync()) {
                          var settingsResult = await settings
                              .copy("$selectedDirectory/flindex_settings.json");
                          if (await settingsResult.exists()) {
                            log("Settings saved successfully");
                          }
                        }
                      }
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content:
                            Text('Insufficient permissions to export data.'),
                      ));
                    }
                  }),
              SettingsTile(
                  leading: const Icon(Icons.file_open),
                  title: const Text('Import data'),
                  description: const Text('Import your flindex data'),
                  onPressed: (_) async {
                    bool load = true;
                    if (await needManageStoragePermission()) {
                      var permission =
                          await Permission.manageExternalStorage.request();
                      log(permission.toString());
                      load = permission.isGranted;
                    } else {
                      var permission = await Permission.storage.request();
                      log(permission.toString());
                      load = permission.isGranted;
                    }
                    if (load) {
                      String? selectedDirectory =
                          await FilePicker.platform.getDirectoryPath();

                      if (selectedDirectory != null) {
                        var targetDir = await localPath();

                        var dataFile =
                            File("$selectedDirectory/flindex_data.json");
                        if (await dataFile.exists()) {
                          log("loading data");
                          dataFile
                              .copy("$targetDir/data.json")
                              .then((file) async {
                            if (await file.exists()) {
                              Provider.of<FlindexData>(context, listen: false)
                                  .loadData();
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(
                                content: Text('Data imported.'),
                              ));
                            }
                          });
                        }

                        var settingsFile =
                            File("$selectedDirectory/flindex_settings.json");
                        if (await settingsFile.exists()) {
                          log("loading settings");
                          settingsFile
                              .copy("$targetDir/settings.json")
                              .then((file) async {
                            if (await file.exists()) {
                              Provider.of<FlindexSettingsData>(context,
                                      listen: false)
                                  .loadData();
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(
                                content: Text('Settings imported.'),
                              ));
                            }
                          });
                        }
                      }
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content:
                            Text('Insufficient permissions to import data.'),
                      ));
                    }
                  }),
            ],
          ),
        ],
      ),
    );
  }
}
