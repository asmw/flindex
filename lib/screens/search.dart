import 'dart:developer';

import 'package:flindex/screens/display.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../flindex_data.dart';

class FlindexSearch extends StatefulWidget {
  const FlindexSearch({Key? key}) : super(key: key);

  @override
  State<FlindexSearch> createState() => _FlindexSearchState();
}

class _FlindexSearchState extends State<FlindexSearch> {
  final _searchInput = TextEditingController();

  List<SearchResult> _searchResults = [];

  @override
  Widget build(BuildContext context) {
    return Consumer<FlindexData>(builder: (context, data, child) {
      return Scaffold(
        appBar: AppBar(
          title: SizedBox(
            width: double.infinity,
            height: 40,
            child: Center(
              child: TextField(
                autofocus: true,
                controller: _searchInput,
                onChanged: (value) {
                  log(value.toString());
                  setState(() {
                    String key = value.toString();
                    if (key == "") {
                      _searchResults = [];
                    } else {
                      _searchResults = data.search(value.toString());
                    }
                  });
                },
                decoration: InputDecoration(
                  filled: true,
                    hintText: 'Search in inventories',
                    fillColor: Theme.of(context).hintColor,
                    prefixIcon: const Icon(Icons.search),
                    suffixIcon: IconButton(
                        icon: const Icon(Icons.clear),
                        onPressed: () {
                          _searchInput.clear();
                          setState(() {
                            _searchResults = [];
                          });
                        })),
              ),
            ),
          ),
        ),
        body: Center(
          child: ListView.builder(
              padding: const EdgeInsets.all(8),
              itemCount: _searchResults.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: EdgeInsets.only(
                      left: _searchResults[index].item == null ? 4 : 16,
                      right: 4,
                      bottom: 1, //_searchResults[index].item == null ? 4 : 1,
                      top: _searchResults[index].item == null ? 8 : 0),
                  child: Ink(
                    decoration: BoxDecoration(
                      borderRadius:
                          const BorderRadius.all(Radius.circular(2.0)),
                      color: Theme.of(context).backgroundColor,
                    ),
                    height: 50,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            settings: const RouteSettings(name: '/display'),
                            builder: (context) => FlindexDisplay(
                                code: _searchResults[index].inventory,
                                highlight: _searchResults[index].item,),
                          ),
                        );
                      },
                      child: Row(children: [
                        const SizedBox(width: 8),
                        Text(_searchResults[index].item == null
                            ? data.prettyName(_searchResults[index].inventory)
                            : _searchResults[index].item!),
                        const Spacer(),
                      ]),
                    ),
                  ),
                );
              }),
        ),
      );
    });
  }
}
