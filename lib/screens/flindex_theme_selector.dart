import 'package:flindex/flindex_settings_data.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:settings_ui/settings_ui.dart';

class FlindexThemeSelector extends StatelessWidget {
  const FlindexThemeSelector({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Select theme')),
      body: SettingsList(
        sections: [
          SettingsSection(tiles: [
            SettingsTile(
              title: const Text("System"),
              trailing: trailingWidget(context, "system"),
              onPressed: (BuildContext context) {
                Provider.of<FlindexSettingsData>(context, listen: false)
                    .setTheme("system");
                Navigator.of(context).pop();
              },
            ),
            SettingsTile(
              title: const Text("Light"),
              trailing: trailingWidget(context, "light"),
              onPressed: (BuildContext context) {
                Provider.of<FlindexSettingsData>(context, listen: false)
                    .setTheme("light");
                Navigator.of(context).pop();
              },
            ),
            SettingsTile(
              title: const Text("Dark"),
              trailing: trailingWidget(context, "dark"),
              onPressed: (BuildContext context) {
                Provider.of<FlindexSettingsData>(context, listen: false)
                    .setTheme("dark");
                Navigator.of(context).pop();
              },
            ),
          ]),
        ],
      ),
    );
  }

  Widget trailingWidget(BuildContext context, String theme) {
    return (Provider.of<FlindexSettingsData>(context).theme == theme)
        ? Icon(Icons.check, color: Theme.of(context).hintColor)
        : const Icon(null);
  }
}
