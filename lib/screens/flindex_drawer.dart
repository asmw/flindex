import 'package:flindex/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:flindex/screens/settings.dart';

class FlindexDrawer extends StatelessWidget {
  const FlindexDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primary,
              ),
              child: Column(children: [
                Padding(
                    padding: const EdgeInsets.all(10),
                    child: Image.asset(
                      "images/icon.png",
                      width: 64,
                      height: 64,
                    )),
                const Text(
                  'Flindex',
                  style: TextStyle(
                    fontSize: 24,
                  ),
                ),
              ])),
          ListTile(
            leading: const Icon(Icons.home),
            title: const Text('Home'),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  settings: const RouteSettings(name: '/'),
                  builder: (_) => const FlindexHome()),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.settings),
            title: const Text('Settings'),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  settings: const RouteSettings(name: '/settings'),
                  builder: (_) => const FlindexSettings()),
            ),
          ),
        ],
      ),
    );
  }
}
