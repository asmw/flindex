import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:path_provider/path_provider.dart';

Future<String> localPath() async {
  Directory? dir;
  try {
    dir = await getExternalStorageDirectory();
  } catch (exception) {
    // Try the alternatives, which should work on all platforms
    dir = await getApplicationSupportDirectory();
  }
  return dir!.path;
}

Future<File> dataFile(String name) async {
  final path = await localPath();
  return File('$path/$name');
}

Future<bool> needManageStoragePermission() async {
  if (Platform.isAndroid) {
    var androidInfo = await DeviceInfoPlugin().androidInfo;
    var release = androidInfo.version.release;
    return release != null && double.parse(release) > 10;
  }
  return false;
}
