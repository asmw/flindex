import 'package:flindex/flindex_data.dart';
import 'package:flindex/flindex_settings_data.dart';
import 'package:flutter/material.dart';
import 'package:flindex/screens/home.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<FlindexData>(create: (context) {
          var data = FlindexData();
          data.loadData();
          return data;
        }),
        ChangeNotifierProvider<FlindexSettingsData>(create: (context) {
          var settings = FlindexSettingsData();
          settings.loadData();
          return settings;
        }),
      ],
      child: const FlindexApp(),
    ),
  );
}

class FlindexApp extends StatelessWidget {
  const FlindexApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flindex',
      themeMode: Provider.of<FlindexSettingsData>(context).themeMode,
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.indigo,
      ),
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const FlindexHome(),
    );
  }
}
