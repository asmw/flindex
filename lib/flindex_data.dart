import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:flindex/flindex_utils.dart';
import 'package:flutter/foundation.dart';

const int currentVersion = 0;
const String dataFileName = "data.json";

class SearchResult {
  SearchResult(this.inventory, [this.item]);
  final String inventory;
  final String? item;
}

class FlindexData with ChangeNotifier {
  dynamic data = {};

  void loadData() async {
    String json = '{}';
    final file = await dataFile(dataFileName);
    log('Loading data from file: $file');
    final exists = await file.exists();
    if (exists) {
      json = await file.readAsString();
    }
    data = jsonDecode(json);
    if (dataVersion() != currentVersion) {
      migrate();
    }
    notifyListeners();
  }

  bool migrate() {
    if (data == null) return false;
    // TODO migrate to items with codes/images
    return true;
  }

  int? dataVersion() {
    if (data != null && data.containsKey('version')) {
      return data['version'];
    }
    return null;
  }

  void saveData() async {
    final file = await dataFile(dataFileName);
    file.open(mode: FileMode.write);
    var payload = jsonEncode(data);
    log('Writing payload to file $file: $payload');
    file.writeAsString(payload);
    notifyListeners();
  }

  void removeCode(String code) {
    data.remove(code);
    saveData();
  }

  void addCode(String code, [String? name]) {
    data[code] = {
      'name': name ?? '',
      'items': [],
    };
    saveData();
  }

  void setName(String code, String name) {
    if (data.containsKey(code)) {
      data[code]['name'] = name;
      saveData();
    } else {
      addCode(code, name);
    }
  }

  int codeCount() {
    return data.length;
  }

  List<SearchResult> search(String key) {
    final lowerKey = key.toLowerCase();
    List<SearchResult> result = [];
    for (String inventory in data.keys) {
      bool parentAdded = false;
      if (inventory.toLowerCase().contains(lowerKey) ||
          data[inventory]['name'].toLowerCase().contains(lowerKey)) {
        result.add(SearchResult(inventory));
        parentAdded = true;
      }
      for (String item in data[inventory]['items']) {
        if (item.toLowerCase().contains(lowerKey)) {
          if (!parentAdded) {
            // Add the parent for a prettier display
            result.add(SearchResult(inventory));
            parentAdded = true;
          }
          result.add(SearchResult(inventory, item));
        }
      }
    }
    return result;
  }

  String? getCode(int index) {
    if (index >= 0 && index < data.length) {
      return data.keys.elementAt(index);
    }
    return null;
  }

  int count(String code) {
    if (data.containsKey(code)) {
      return data[code]['items'].length;
    }
    return 0;
  }

  dynamic items(String code, int index) {
    if (index >= 0 && index < data[code]['items'].length) {
      return data[code]['items'][index];
    }
    return {};
  }

  String getName(String code) {
    if (data.containsKey(code)) {
      return data[code]['name'];
    }
    return '';
  }

  bool addItem(String code, String item) {
    if (!data.containsKey(code)) {
      addCode(code);
    }
    if (!data[code]['items'].contains(item)) {
      data[code]['items'].add(item);
      saveData();
      return true;
    }
    return false;
  }

  void removeItem(String code, String item) {
    data[code]['items'].remove(item);
    saveData();
  }

  bool contains(String code) {
    return data.containsKey(code);
  }

  String prettyName(String? code) {
    if (code != null) {
      if (data.containsKey(code)) {
        return '${data[code]['name']} ($code)';
      } else {
        return code;
      }
    }
    return '';
  }
}
