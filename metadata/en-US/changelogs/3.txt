UI:
- Use better delete icon in list editor
- Make item boxes less rounded
Misc:
- Fastlane metadata added
- Multidex build works on console and in VScodium now
